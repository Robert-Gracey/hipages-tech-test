Your Solution Documentation
===========================

<a href="https://gitlab.com/Robert-Gracey/hipages-tech-test/-/pipelines"><img src="https://gitlab.com/Robert-Gracey/hipages-tech-test/badges/master/pipeline.svg" alt="Pipeline status" /></a>

## Running the app
Run `docker-compose up` from the project root.

Frontend (client) can be then be reached at http://localhost:3000

Backend (server) can be reached at http://localhost:3001

Backend exposes the following endpoints:
- GET `/` - default NestJS route
- GET `/jobs` - returns all jobs in the database
- PUT `/jobs/[id]` - Update the status of a specific job

### Note
There is an artifical delay introduced on the client side so the loading spinner is visible while the app loads data. This can be configured by adjusting setting the environment variable `REACT_APP_DELAY` for the client project (time is in milliseconds. Default 1000ms)

## Test
Ensure dependencies are installed (`npm install`) for each project, then from either client or server directory, run `npm run test`.

## Technologies
### Frontend
Client project scaffolded with Create-React-App (CRA) as the task stipulated an SPA. CRA provides a ready to develop frontend build pipeline with hot-reloading without having to setup a toolchain yourself. This seemed like the best choice for expediency. CRA also allows you to "eject" configurations to fine tune the build pipeline should you want to, although this wasn't necessary for this task.

### Backend
#### NestJS
NestJS was used to structure the backend as it encourages the SOLID design principles. Similar to CRA, NestJS also provides CLI tooling which allowed the backend project to be setup quickly.

#### TypeORM
TypeORM was used to abstract database interactions as it provides type safety, sanitised queries and datastore agnosticism. For this task in particular, TypeORM made data interactions trivial and would allow for further development of such an app in the future.

### GitLab
Gitlab was chosen as it has the CI/CD pipeline built in.

## Next Steps
Given more time I'd implement the following:
- Improve test suite:
  - More e2e tests
  - More unit tests
- Improve monitoring (e.g. add metrics endpoint)
- Add deployment steps to CI/CD pipeline (e.g. deploy to a cloud service GKE, AKS, Amazon EKS)
- Improve backend code:
  - Add pagination to queries
  - Flesh out other endpoints (suburb, category) so their data can be retrieved individually
  - Allow filtering of data (e.g. request only "new" status jobs or request all jobs in a certain postcode)
