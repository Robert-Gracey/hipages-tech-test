import React, { useState } from 'react';
import JobsList from '../jobsList/JobsList';
import { Job } from '../jobCard/types';
import useAsync, { STATE } from '../../hooks/useAsync';
import JobDAO from '../../util/job.dao';
import Loader from '../loader/Loader';
import { JobStatus } from '../../util/JobStatus';

const FILTER_STATES = [
  { label: 'invited', filter: JobStatus.New },
  { label: 'accepted', filter: JobStatus.Accepted },
];

const jobDao = JobDAO(
  process.env.REACT_APP_API_URL || 'http://localhost:3001/jobs',
);

/**
 * Filters an array of jobs and returns only those whose status matches
 * @param status The job status to filter for
 */
const filterJobs = (status: string, jobs: Array<Job>) => {
  return jobs.filter((job) => {
    return job.status === status;
  });
};

const tabButtonStyles = (isActive: boolean): string => {
  return [
    'w-1/2',
    'py-4',
    'capitalize',
    'text-lg',
    'bg-white',
    'border-b-4',
    'shadow',
    'border-white',
    isActive ? 'font-bold' : 'font-medium',
    isActive ? 'border-orange-500' : 'hover:border-gray-300',
  ].join(' ');
};

// TODO replace
const updateJobState = (state: any, id: number, status: string) => {
  return state.map((job: Job) => {
    if (job.id === id) {
      job.status = status;
    }
    return job;
  });
};

// Displays a filtered array of jobs
const FilterableJobsList = () => {
  const { state, data, setData } = useAsync(jobDao.fetchAll());
  const [currentFilter, setCurrentFilter] = useState(FILTER_STATES[0].filter);

  // TODO - Refactor?
  const handler = async (id: number, status: string) => {
    if (await jobDao.update(id, status)) {
      setData((prevState: any) => {
        return updateJobState(prevState, id, status);
      });
    }
  };

  return (
    <div>
      <div className="mb-6 sticky top-0">
        {FILTER_STATES.map((filterState, index) => {
          return (
            <button
              key={index}
              className={tabButtonStyles(currentFilter === filterState.filter)}
              onClick={() => {
                setCurrentFilter(filterState.filter);
              }}
            >
              {filterState.label}
            </button>
          );
        })}
      </div>
      {state === STATE.PENDING && (
        <div className="mt-14 text-center">
          <Loader />
        </div>
      )}
      {state === STATE.SUCCESS && (
        <JobsList jobs={filterJobs(currentFilter, data)} handler={handler} />
      )}
    </div>
  );
};

export default FilterableJobsList;
