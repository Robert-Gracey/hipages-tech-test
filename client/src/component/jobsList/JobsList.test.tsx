import React from 'react';
import JobsList from './JobsList';
import renderer from 'react-test-renderer';
import JobCard from '../jobCard/JobCard';

const mockProps = {
  job: {
    id: 0,
    status: 'new',
    suburb: {
      name: 'Zetland',
      postcode: '2017',
    },
    category: 'plubming',
    contactName: 'Robert Gracey',
    contactPhone: '1234567890',
    contactEmail: 'example@example.com',
    price: 25,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam nulla labore voluptas aliquam laboriosam quis quia vitae voluptatibus adipisci, vel nam maiores tempore ',
    createdAt: new Date('2020/05/05'),
    updatedAt: new Date('2020/06/06'),
  },
  handler: () => {
    return null;
  },
};

test('renders a list of jobs', () => {
  // Render jobs list with NO jobs to display
  let component = renderer.create(
    <JobsList jobs={[]} handler={mockProps.handler} />,
  );

  // Ensure it matches snapshot
  expect(component.toJSON()).toMatchSnapshot();

  // Render jobs list WITH jobs to display
  component = renderer.create(
    <JobsList
      jobs={[mockProps.job, mockProps.job, mockProps.job]}
      handler={mockProps.handler}
    />,
  );

  // Ensure it matches snapshot
  expect(component.toJSON()).toMatchSnapshot();
});
