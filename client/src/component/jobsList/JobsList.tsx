import React from 'react';
import JobCard from '../jobCard/JobCard';
import { Job } from '../jobCard/types';

interface JobsListProps {
  jobs: Array<Job>;
  handler: (id: number, status: string) => void;
}

// Displays an array of jobs
const JobsList = (props: JobsListProps) => {
  const { jobs, handler } = props;

  return (
    <div>
      {jobs.length <= 0 && (
        <div className="text-center text-2xl font-bold">No jobs to display</div>
      )}
      {jobs.map((job: Job, i: number) => {
        return <JobCard key={i} job={job} handler={handler} />;
      })}
    </div>
  );
};

export default JobsList;
