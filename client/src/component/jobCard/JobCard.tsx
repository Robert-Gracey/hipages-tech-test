import React from 'react';
import { Job } from './types';
import moment from 'moment';
import { JobStatus } from '../../util/JobStatus';

interface JobCardProps {
  job: Job;
  handler: (id: number, status: string) => void;
}

const DATE_FORMAT = 'MMMM D YYYY @ h:mm a';

const getFirstLetter = (s: string): string | null => {
  return s.length > 0 ? s.charAt(0) : s;
};

const isAccepted = (status: string) => {
  return status === JobStatus.Accepted;
};

const JobCard = (props: JobCardProps) => {
  const { job, handler } = props;

  const Description = () => {
    return (
      <div className="pt-3 pb-5 text-gray-600 text-xs">{job.description}</div>
    );
  };

  const NewFooter = () => {
    // Footer for leads that HAVE NOT been accepted
    return (
      <div>
        <div className="px-6 border-b border-gray-300">
          <Description />
        </div>
        <div className="px-6 py-3">
          <button
            className="px-5 py-2 font-bold text-white text-sm bg-orange-500 border-b-4 border-orange-700 mr-4 hover:bg-orange-600"
            onClick={() => handler(job.id, JobStatus.Accepted)}
          >
            Accept
          </button>

          <button
            className="px-5 py-2 font-bold text-gray-700 text-sm bg-gray-400 border-b-4 border-gray-700 mr-6 hover:bg-gray-600 hover:text-white"
            onClick={() => handler(job.id, JobStatus.Declined)}
          >
            Decline
          </button>

          <span className="text-gray-600">
            <span className="font-bold mr-2">${job.price}</span>
            Lead Invitation
          </span>
        </div>
      </div>
    );
  };

  const AcceptedFooter = () => {
    // Footer for leads that HAVE already been accepted
    return (
      <div className="px-6 py-3">
        <div>
          <span className="mr-4">
            <i
              className="fa fa-phone text-xs text-gray-600 mr-1"
              aria-hidden="true"
            />
            <span className="font-bold font-lg text-orange-500">
              {job.contactPhone}
            </span>
          </span>
          <span>
            <i
              className="fa fa-envelope-o text-xs text-gray-600 mr-1"
              aria-hidden="true"
            />
            <span className="font-bold font-lg text-orange-500">
              {job.contactEmail}
            </span>
          </span>
        </div>
        <Description />
      </div>
    );
  };

  return (
    <div className="w-full bg-white shadow mb-5">
      <div className="border-b border-gray-300 py-3 pl-6">
        <div className="rounded-full bg-orange-500 py-1 px-3 inline-block text-xl text-white uppercase mr-4 align-top font-bold">
          {getFirstLetter(job.contactName)}
        </div>

        <div className="inline-block">
          <div className="text-sm font-bold capitalize">{job.contactName}</div>
          <div className="text-xs text-gray-600">
            {moment(job.createdAt).format(DATE_FORMAT)}
          </div>
        </div>
      </div>

      <div className="px-6 border-b border-gray-300 py-3 text-gray-600">
        <span className="mr-4">
          <i className="fa fa-map-marker mr-2" aria-hidden="true" />
          <span className="mr-1">{job.suburb.name}</span>
          <span>{job.suburb.postcode}</span>
        </span>

        <span className="mr-4">
          <i className="fa fa-suitcase mr-2" aria-hidden="true" />
          {job.category}
        </span>

        <span className="mr-4">Job ID: {job.id}</span>
        {isAccepted(job.status) && <span>${job.price} Lead Invitation</span>}
      </div>

      {isAccepted(job.status) ? <AcceptedFooter /> : <NewFooter />}
    </div>
  );
};

export default JobCard;
