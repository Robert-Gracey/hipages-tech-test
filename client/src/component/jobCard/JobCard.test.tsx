import React from 'react';
import JobCard from './JobCard';
import renderer from 'react-test-renderer';

const mockProps = {
  job: {
    id: 0,
    status: 'new',
    suburb: {
      name: 'Zetland',
      postcode: '2017',
    },
    category: 'plubming',
    contactName: 'Robert Gracey',
    contactPhone: '1234567890',
    contactEmail: 'example@example.com',
    price: 25,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam nulla labore voluptas aliquam laboriosam quis quia vitae voluptatibus adipisci, vel nam maiores tempore ',
    createdAt: new Date('2020/05/05'),
    updatedAt: new Date('2020/06/06'),
  },
  handler: () => {
    return null;
  },
};

test('renders job card', () => {
  const component = renderer.create(
    <JobCard job={mockProps.job} handler={mockProps.handler} />,
  );

  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
