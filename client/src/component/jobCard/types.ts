export type Job = {
  id: number;
  status: string;
  suburb: {
    name: string;
    postcode: string;
  };
  category: string;
  contactName: string;
  contactPhone: string;
  contactEmail: string;
  price: number;
  description: string;
  createdAt: Date;
  updatedAt: Date;
};
