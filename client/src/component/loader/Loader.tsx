import React from 'react';
import './Loader.css';

const Loader = () => {
  return <div className="inline-block loader"></div>;
};

export default Loader;
