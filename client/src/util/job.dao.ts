import axios from 'axios';
import { Job } from '../component/jobCard/types';

const SUCCESS = 200;

export default (baseUrl: string) => {
  return {
    fetchAll: async (): Promise<Array<Job>> => {
      try {
        const response = (await axios.get(baseUrl)).data;

        return response.map((job: any) => {
          // Reshape data slightly so it fits Job type definition
          job.suburb = {
            name: job.suburb.name,
            postcode: job.suburb.postcode,
          };
          job.category = job.category.name;

          return job;
        });
      } catch (error) {
        console.error(error);
        return [];
      }
    },

    update: async (id: number, status: string): Promise<boolean> => {
      try {
        const response = await axios.put(
          baseUrl + '/' + id,
          { status: status },
          { headers: { 'Content-Type': 'application/json' } },
        );

        return response.status === SUCCESS;
      } catch (error) {
        console.error(error);
        return false;
      }
    },
  };
};
