// code from: https://usehooks.com/useAsync/

import { useState, useEffect, useCallback } from 'react';

// Current state of the async function
export enum STATE {
  NOTEXECUTED,
  PENDING,
  SUCCESS,
  ERROR,
}

const useAsync = (
  asyncFunction: Promise<any>,
  immediate = true,
): {
  execute: () => void;
  state: STATE;
  data: any;
  error: any;
  setData: React.SetStateAction<any>;
} => {
  const [state, setState] = useState(STATE.NOTEXECUTED);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  const execute = useCallback(async () => {
    setState(STATE.PENDING);
    setData(null);
    setError(null);

    try {
      setData(await asyncFunction);
      setTimeout(() => {
        setState(STATE.SUCCESS);
      }, Number.parseInt(process.env.REACT_APP_DELAY || '1000'));
    } catch (error) {
      setError(error);
      setState(STATE.ERROR);
    }
  }, [asyncFunction]);

  useEffect(() => {
    if (immediate && state === STATE.NOTEXECUTED) {
      execute();
    }
  }, [execute, immediate, state]);

  return { execute, state, data, error, setData };
};

export default useAsync;
