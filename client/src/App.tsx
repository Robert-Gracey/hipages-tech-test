import React from 'react';
import './App.css';
import 'font-awesome/css/font-awesome.min.css';
import FilterableJobsList from './component/filterableJobsList/FilterableJobsList';

const App = () => {
  return (
    <div className="w-3/4 mx-auto py-2 px-2">
      <FilterableJobsList />
    </div>
  );
};

export default App;
