ARG PORT_ARG=3000

FROM node:12.18.2-alpine

LABEL maintainer="Robert Gracey <robert.c.gracey@gmail.com>"

ENV PORT=${PORT_ARG}

# Create app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

RUN npm run build

EXPOSE ${PORT}

CMD [ "npm", "start" ]
