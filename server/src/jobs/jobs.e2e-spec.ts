import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { JobsService } from './jobs.service';
import { INestApplication } from '@nestjs/common';
import { JobsController } from './jobs.controller';

describe('Jobs', () => {
  let app: INestApplication;
  const jobsService = {
    findAll: () => ['job', 'job', 'job'],
    update: () => {
      return { status: 'accepted' };
    },
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [JobsController],
      providers: [JobsService],
    })
      .overrideProvider(JobsService)
      .useValue(jobsService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET jobs`, () => {
    return request(app.getHttpServer())
      .get('/jobs')
      .expect(200)
      .expect(jobsService.findAll());
  });

  it(`/PUT jobs/0`, () => {
    return request(app.getHttpServer())
      .put('/jobs/0')
      .set('Accept', 'application/json')
      .send({ status: 'accepted' })
      .expect(200)
      .expect(jobsService.update());
  });

  afterAll(async () => {
    await app.close();
  });
});
