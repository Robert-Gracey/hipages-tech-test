import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import Job from './entity/job.entity';

@Injectable()
export class JobsService {
  constructor(
    @InjectRepository(Job)
    private jobsRepository: Repository<Job>,
  ) {}

  findAll(): Promise<Job[]> {
    return this.jobsRepository.find();
  }

  async update(id: number, status: string): Promise<Job> {
    const job: Job = await this.jobsRepository.findOne(id);
    job.status = status;
    return this.jobsRepository.save(job);
  }
}
