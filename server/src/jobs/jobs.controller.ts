import {
  Controller,
  Get,
  Put,
  Param,
  Body,
  HttpException,
  HttpStatus,
  ParseIntPipe,
  ValidationPipe,
  Logger,
} from '@nestjs/common';
import { JobsService } from './jobs.service';
import Job from './entity/job.entity';
import UpdateJobDto from './dto/update-job.dto';

@Controller('jobs')
export class JobsController {
  constructor(private readonly jobsService: JobsService) {}

  @Get()
  findAll(): Promise<Job[]> {
    return this.jobsService.findAll();
  }

  @Put(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body(new ValidationPipe()) updateJobDto: UpdateJobDto,
  ): Promise<Job> {
    try {
      Logger.log(`Updating status of job id: ${id} to ${updateJobDto.status}`);
      return this.jobsService.update(id, updateJobDto.status);
    } catch (error) {
      Logger.error(
        `Error updating status of job id: ${id} to ${updateJobDto.status}. See stack trace below for more information`,
        error.toString(),
      );
      throw new HttpException(
        'Internal Server Error',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
