import { IsEnum } from 'class-validator';
import { JobStatus } from './JobStatus';

export default class UpdateJobDto {
  @IsEnum(JobStatus)
  status: JobStatus;
}
