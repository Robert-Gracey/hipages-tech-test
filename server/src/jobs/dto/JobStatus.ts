export enum JobStatus {
  New = 'new',
  Accepted = 'accepted',
  Declined = 'declined',
}
