import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'suburbs' })
export default class Suburb {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  postcode: string;
}
