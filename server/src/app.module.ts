import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobsModule } from './jobs/jobs.module';
import { HealthModule } from './health/health.module';

@Module({
  imports: [TypeOrmModule.forRoot(), JobsModule, HealthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
